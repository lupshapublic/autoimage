#!/bin/bash

# feel free to change this section
#DEFAULT_ISO_URL=http://pistorage.florida/ISOs/2024.01.17/jammy-live-server-amd64.iso
DEFAULT_ISO_URL=https://cdimage.ubuntu.com/ubuntu-server/jammy/daily-live/current/jammy-live-server-amd64.iso

DEFAULT_SERVERNAME=myserver
DEFAULT_USER=ubuntu
DEFAULT_PASSWORD=ubuntu
# end of customizable section

read -p "Ubuntu ISO URL [Default: $DEFAULT_ISO_URL]: " ISO_URL
if [ -z "$ISO_URL" ]; then
  ISO_URL=$DEFAULT_ISO_URL
fi

read -p "New server name [Default: $DEFAULT_SERVERNAME]: " SERVERNAME
if [ -z "$SERVERNAME" ]; then
  SERVERNAME=$DEFAULT_SERVERNAME
fi

read -p "User to create [Default: $DEFAULT_USER]: " USER
if [ -z "$USER" ]; then
  USER=$DEFAULT_USER
fi

read -p "Password to set for user $USER [Default: $DEFAULT_PASSWORD]: " PASSWORD
if [ -z "$PASSWORD" ]; then
  PASSWORD=$DEFAULT_PASSWORD
fi

banner() {
    local text="$@"
    local length=${#text}
    echo "╔$(printf '═%.0s' $(seq 1 $length))╗"
    while IFS= read -r line; do
        printf "║%s║\n" "$line"
    done <<< "$text"
    echo "╚$(printf '═%.0s' $(seq 1 $length))╝"
}

SUMMARY="ISO_URL:$ISO_URL
SERVERNAME:$SERVERNAME
USER:$USER
PASSWORD:$PASSWORD"
banner $SUMMARY


banner "installing necessary utils"
sudo apt install -y xorriso whois p7zip-full wget

# create needed dirs
if [ -d "./source-files" ]; then
  banner "purging old ./source-files dir"
  rm -r "./source-files"
fi
mkdir -p ./source-files/server
touch ./source-files/server/user-data
touch ./source-files/server/meta-data

# get a copy of the original live server iso and place it in the home ISOs directory
mkdir -p ./ISOs
ISOFILENAME=$(basename $ISO_URL)
if [ -e "./ISOs/${ISOFILENAME}" ]; then
  banner "ISO already downloaded: ./ISOs/${ISOFILENAME}"
  ls -la ./ISOs/${ISOFILENAME}
else
  banner "ISO not present, downloading $ISOFILENAME now from $ISO_URL"
  wget -P ./ISOs/ $ISO_URL
fi

# unzip the original live iso
banner "unzipping $ISOFILENAME into ./source-files"
7z -y x ./ISOs/$ISOFILENAME -osource-files

echo "moving the BOOT dir one dir up"
cd source-files
mv -f '[BOOT]' ../BOOT
cd ..

banner "creating SHA 512 password"
PASSHASH=`mkpasswd --method=SHA-512 $PASSWORD`

banner "creating user-data file ./source-files/server/user-data"
cat <<EOF > ./source-files/server/user-data
#cloud-config
autoinstall:
  version: 1
  storage:
    layout:
      name: lvm
      match:
        size: largest
  locale: en_US.UTF-8
  keyboard:
    layout: us
  identity:
    hostname: $SERVERNAME
    password: $PASSHASH
    username: $USER
  packages:
    - nano
EOF

banner "updating grub in ./source-files/boot/grub/grub.cfg"
cat <<EOF > ./source-files/boot/grub/grub.cfg
set timeout=3
loadfont unicode
set menu_color_normal=white/black
set menu_color_highlight=black/light-gray
menuentry "Autoinstall Ubuntu Server" {
        set gfxpayload=keep
        linux   /casper/vmlinuz quiet autoinstall ds=nocloud\;s=/cdrom/server/  ---
        initrd  /casper/initrd
}
EOF

# look at the custom config once
banner "Your user-data file will look as follows:"
cat ./source-files/server/user-data

banner "Now building you a ./buildimage.sh script that you can edit later"
cat <<EOF > ./buildimage.sh
#!/bin/bash
cd ./source-files/
xorriso -as mkisofs -r \
-V 'Ubuntu 22.04 LTS AUTO (EFIBIOS)' \
-o ../ISOs/AUTOINSTALL-${ISOFILENAME} \
--grub2-mbr ../BOOT/1-Boot-NoEmul.img \
-partition_offset 16 \
--mbr-force-bootable \
-append_partition 2 28732ac11ff8d211ba4b00a0c93ec93b ../BOOT/2-Boot-NoEmul.img \
-appended_part_as_gpt \
-iso_mbr_part_type a2a0d0ebe5b9334487c068b6b72699c7 \
-c '/boot.catalog' \
-b '/boot/grub/i386-pc/eltorito.img' \
-no-emul-boot -boot-load-size 4 -boot-info-table --grub2-boot-info \
-eltorito-alt-boot \
-e '--interval:appended_partition_2:::' \
--no-emul-boot \
.
cd ../ISOs
echo "*******************************************************************************************"
echo "Your autoinstall ISO has been created: AUTOINSTALL-${ISOFILENAME}"
echo "*******************************************************************************************"
pwd
ls -la
EOF

# make it executable
chmod u+x ./buildimage.sh

# build the final ISO image
banner "execute ./buildimage.sh to build your custom ISO"

