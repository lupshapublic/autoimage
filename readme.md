# Ubuntu unattended install ISO

This script downloads the Ubuntu Jammy live server amd64 iso image, unpacks it, uses your settings, and creates a new iso image that you can use to run an unattended install. 

# How to run it

```
git clone https://gitlab.com/lupshapublic/autoimage
cd autoimage
chmod u+x *.sh
./configure.sh
```

Follow the prompts in the configuration, or just press ENTER for defaults.

When the configuration is complete, run
```
./buildimage.sh
```


